package com.daniel.prog.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {
    private String ip;
    private String db;
    private String user;
    private String password;
    private Connection connection;


    public MySQLConnection(String ip, String db, String user, String password) {
        this.ip = ip;
        this.db = db;
        this.user = user;
        this.password = password;
        this.loadDriver();
    }

    public MySQLConnection(String db, String user, String password) {
        this.ip = "localhost";
        this.db = db;
        this.user = user;
        this.password = password;
        this.loadDriver();
    }

    public Connection getConnection() {

        if (this.connection == null) {

            this.connect();

        }

        return connection;

    }

    public void close(Connection conn) {

        try {

            conn.close();

        } catch (SQLException e) {

            e.printStackTrace();
        }

    }


    private void connect() {

        try {

            this.connection = DriverManager.getConnection(getUrlConnection(), this.user, this.password);

        } catch (SQLException ex) {
            System.out.println("SQLException " + ex.getMessage());
            System.out.println("SQLState " + ex.getSQLState());
            System.out.println("VendorError " + ex.getErrorCode());
        }


    }

    private void loadDriver() {

        try {

            final Object o = Class.forName("com.mysql.cj.jdbc.Driver").newInstance();


        } catch (Exception ex) {

            System.out.println("Error loading driver: " + ex.getMessage());

        }

    }

    private String getUrlConnection() {

        return "jdbc:mysql://" + this.ip + "/" + this.db + "?serverTimezone=UTC&useSSL=false";

    }
}
