package com.daniel.prog.view;

import com.daniel.prog.model.User;
import com.daniel.prog.model.DbUserRepository;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class UserListControllerView {

    @FXML
    private TableView<User> userTable;
    @FXML
    private TableColumn<User, String> firstNameColumn;
    @FXML
    private TableColumn<User, String> lastNameColumn;
    @FXML
    private TextField searchTextField;
    @FXML
    private Button searchButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button previousButton;
    @FXML
    private Label pageNumberLabel;
    @FXML
    private Label idLabel;
    @FXML
    private Label firstNameLabel;
    @FXML
    private Label lastNameLabel;
    @FXML
    private Label emailLabel;
    @FXML
    private Label phoneNumberLabel;
    @FXML
    private Label activeLabel;
    @FXML
    private Label lastLoginLabel;
    @FXML
    private Label birthdayLabel;

    private DbUserRepository userRepository;

    private int pageNum = 0;
    //this value defines how many users are going to be shown in every page
    private final int pageSize=3;
    private final int userCount;

    public UserListControllerView(DbUserRepository userRepository){
        this.userRepository = userRepository;
        this.userCount=userRepository.getUserCount();
    }
    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        if (pageSize>=userCount){

            previousButton.setDisable(true);
            nextButton.setDisable(true);


        }else {

            previousButton.setDisable(true);

        }

        userTable.setItems(getUserList());

        int page = (pageNum*pageSize)+pageSize;

        if (pageSize>userCount) {
            pageNumberLabel.setText("mostrando " + userCount + " de " + userCount);
        }else {
            pageNumberLabel.setText("mostrando " + page + " de " + userCount);
        }

        searchButtonOnClick();
        nextButtonOnClick();
        previousButtonOnClick();

        firstNameColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getFirstName()));
        lastNameColumn.setCellValueFactory((cellData) -> new ReadOnlyStringWrapper(cellData.getValue().getLastName()));

        // Listen for selection changes and show the person details when changed.
        userTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showUserDetails(newValue)
        );
    }
    public ObservableList<User> getUserList() {
        return FXCollections.observableArrayList(this.userRepository.findUserByEnterprisePaginado(pageNum,pageSize));
    }
    public ObservableList<User> getUserListBySearch(String searchBy) {
        return FXCollections.observableArrayList(this.userRepository.findUserBySearchOnDb(searchBy));
    }

    private void showUserDetails(User user){
        if (user==null){
            idLabel.setText("");
            firstNameLabel.setText("");
            lastNameLabel.setText("");
            phoneNumberLabel.setText("");
            lastLoginLabel.setText("");
            activeLabel.setText("");
            birthdayLabel.setText("");
        }else {
            idLabel.setText(user.getId().toString());
            firstNameLabel.setText(user.getFirstName());
            lastNameLabel.setText(user.getLastName());
            phoneNumberLabel.setText(user.getPhoneNumber());
            lastLoginLabel.setText(user.getLastLogin().toString());
            activeLabel.setText(user.getActiveString());
            birthdayLabel.setText(user.getBirthday().toString());
        }
    }

    public String getSearchTextfield(){
        return searchTextField.getText();
    }

    public void increasePage(){
        pageNum++;
    }
    public void decreasePage(){
        pageNum--;
    }

    public void nextButtonOnClick(){
        increasePage();
        nextButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                increasePage();

                userTable.setItems(getUserList());

                int page = (pageNum*pageSize)+pageSize;

                pageNumberLabel.setText("mostrando "+ page +" de "+userCount);

                if (page>=userCount){
                    pageNumberLabel.setText("mostrando "+ userCount +" de "+userCount);
                    nextButton.setDisable(true);
                }

                if (previousButton.isDisabled()){
                    previousButton.setDisable(false);
                }

            }

        });

    }


    public void previousButtonOnClick(){
        decreasePage();
        previousButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {

                decreasePage();

                userTable.setItems(getUserList());

                int page = (pageNum*pageSize)+pageSize;

                pageNumberLabel.setText("mostrando "+ page +" de "+userCount);

                if (pageNum ==0){
                    previousButton.setDisable(true);
                }

                if (nextButton.isDisabled()){
                    nextButton.setDisable(false);
                }

            }

        });

    }

    public void searchButtonOnClick(){
        searchButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                userTable.setItems(getUserListBySearch(getSearchTextfield()));
            }

        });

    }

}
