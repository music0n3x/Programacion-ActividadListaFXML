package com.daniel.prog.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class User {

    private Integer id;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;
    private String password;
    private String roles;
    private boolean active;
    private String salt;
    private LocalDateTime createdOn;
    private LocalDateTime lastLogin;
    private String locale;
    private int idEnterprise;
    private LocalDate birthday;

    public User(String firstName,String lastName,String email,String phoneNumber,String password,LocalDate birthday){

        this.id=0;
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
        this.phoneNumber=phoneNumber;
        this.password=password;
        this.active=true;
        this.salt=null;
        this.createdOn=LocalDateTime.now();
        this.lastLogin=LocalDateTime.now();
        this.locale="es_Es";
        this.idEnterprise=1;
        this.birthday=birthday;

    }

    public User(int id, String firstName, String lastName, String email, String phoneNumber, String password, String roles, boolean active, String salt, LocalDateTime createdOn, LocalDateTime lastLogin, String locale, int idEnterprise, LocalDate birthday) {

        this.id=id;
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
        this.phoneNumber=phoneNumber;
        this.password=password;
        this.active=active;
        this.salt=salt;
        this.createdOn=createdOn;
        this.lastLogin=lastLogin;
        this.locale=locale;
        this.idEnterprise=idEnterprise;
        this.birthday=birthday;


    }

    public LocalDateTime getCreatedOn() {
        return createdOn;
    }

    public String getLocale() {
        return locale;
    }

    public Integer getId() {
        return id;
    }

    public boolean getActive(){
        return active;
    }

    public String getActiveString(){
        if (active){
            return "true";
        }
        return "false";
    }

    public int getIdEnterprise() {
        return idEnterprise;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public LocalDateTime getLastLogin() {
        return lastLogin;
    }

    public String getRoles() {
        return roles;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getSalt() {
        return salt;
    }

    public String getUsername() {
        return email;
    }

}
