package com.daniel.prog.model;

import com.daniel.prog.connection.MySQLConnection;

import java.sql.*;
import java.util.ArrayList;

public class DbUserRepository {

    private int pageSize;
    private MySQLConnection mySQLConnection;

    public DbUserRepository(MySQLConnection mySQLConnection){

        this.pageSize=pageSize;
        this.mySQLConnection=mySQLConnection;

    }

    public boolean save(User user){

        if (user.getId() != null){
            return this.update(user);
        }

        return this.add(user);

    }

    public User findUserById(int id){

        Connection connection = this.mySQLConnection.getConnection();
        String sql = "SELECT * FROM User where id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
            return new User(resultSet.getInt("id"),resultSet.getString("firstName"),resultSet.getString("lastName"),resultSet.getString("email"),resultSet.getString("phoneNumber"),resultSet.getString("password"),resultSet.getString("roles"),resultSet.getBoolean("active"),resultSet.getString("salt"),resultSet.getTimestamp("createdOn").toLocalDateTime(),resultSet.getTimestamp("lastLogin").toLocalDateTime(),resultSet.getString("locale"),resultSet.getInt("idEnterprise"),resultSet.getTimestamp("birthday").toLocalDateTime().toLocalDate());
            }
            return null;

        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;
    }

    public User getUserById(int id) throws UserNotFoundException {

        User user = findUserById(id);

        if (user != null){
            return user;
        }

        throw new UserNotFoundException();

    }


    public User findUserByEmail(String email){

        Connection connection = this.mySQLConnection.getConnection();
        String sql = "SELECT * FROM User where email=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,email);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()){
                return new User(resultSet.getInt("id"),resultSet.getString("firstName"),resultSet.getString("lastName"),resultSet.getString("email"),resultSet.getString("phoneNumber"),resultSet.getString("password"),resultSet.getString("roles"),resultSet.getBoolean("active"),resultSet.getString("salt"),resultSet.getTimestamp("createdOn").toLocalDateTime(),resultSet.getTimestamp("lastLogin").toLocalDateTime(),resultSet.getString("locale"),resultSet.getInt("idEnterprise"),resultSet.getTimestamp("birthday").toLocalDateTime().toLocalDate());
            }
            return null;

        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;
    }

    public ArrayList<User> findUserByEnterprise(int idEnterprise, int page){

        ArrayList<User> arrayUsers = new ArrayList<User>();

        Connection connection = this.mySQLConnection.getConnection();
        String sql = "SELECT * FROM User where idEnterprise=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,idEnterprise);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                User user = new User(resultSet.getInt("id"),resultSet.getString("firstName"),resultSet.getString("lastName"),resultSet.getString("email"),resultSet.getString("phoneNumber"),resultSet.getString("password"),resultSet.getString("roles"),resultSet.getBoolean("active"),resultSet.getString("salt"),resultSet.getTimestamp("createdOn").toLocalDateTime(),resultSet.getTimestamp("lastLogin").toLocalDateTime(),resultSet.getString("locale"),resultSet.getInt("idEnterprise"),resultSet.getTimestamp("birthday").toLocalDateTime().toLocalDate());
                arrayUsers.add(user);

            }
            return arrayUsers;

        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;

    }

    public ArrayList<User> findUserByEnterprisePaginado(int pageNum,int pageSize){

        ArrayList<User> arrayUsers = new ArrayList<User>();

        Connection connection = this.mySQLConnection.getConnection();
        String sql = "SELECT * FROM User LIMIT ?,?";

        int offset = pageNum*pageSize;

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,offset);
            preparedStatement.setInt(2,pageSize);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                User user = new User(resultSet.getInt("id"),resultSet.getString("firstName"),resultSet.getString("lastName"),resultSet.getString("email"),resultSet.getString("phoneNumber"),resultSet.getString("password"),resultSet.getString("roles"),resultSet.getBoolean("active"),resultSet.getString("salt"),resultSet.getTimestamp("createdOn").toLocalDateTime(),resultSet.getTimestamp("lastLogin").toLocalDateTime(),resultSet.getString("locale"),resultSet.getInt("idEnterprise"),resultSet.getTimestamp("birthday").toLocalDateTime().toLocalDate());
                arrayUsers.add(user);

            }
            return arrayUsers;

        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;

    }

    public ArrayList<User> findUserBySearchOnDb(String searchBy){

        String searchByDone="%"+searchBy+"%";

        ArrayList<User> arrayUsers = new ArrayList<User>();

        Connection connection = this.mySQLConnection.getConnection();
        String sql = "SELECT * FROM User where idEnterprise=1 and " +
                "firstName like ? " +
                "or lastName like ?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,searchByDone);
            preparedStatement.setString(2,searchByDone);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                User user = new User(resultSet.getInt("id"),resultSet.getString("firstName"),resultSet.getString("lastName"),resultSet.getString("email"),resultSet.getString("phoneNumber"),resultSet.getString("password"),resultSet.getString("roles"),resultSet.getBoolean("active"),resultSet.getString("salt"),resultSet.getTimestamp("createdOn").toLocalDateTime(),resultSet.getTimestamp("lastLogin").toLocalDateTime(),resultSet.getString("locale"),resultSet.getInt("idEnterprise"),resultSet.getTimestamp("birthday").toLocalDateTime().toLocalDate());
                arrayUsers.add(user);

            }
            return arrayUsers;

        }catch (SQLException e){
            e.printStackTrace();
        }

        return null;

    }

    public int getUserCount(){

        Connection connection = this.mySQLConnection.getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("Select count(*) AS count FROM User");
            while (resultSet.next()){
                return resultSet.getInt("count");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public boolean dropUser(int id){

        Connection connection = this.mySQLConnection.getConnection();
        String sql = "DELETE FROM User WHERE id=?";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();

            return true;

        }catch (SQLException e){
            e.printStackTrace();
        }

        return false;

    }

    private boolean add(User user){


        Connection connection = this.mySQLConnection.getConnection();
        String sql = "INSERT INTO USER(firstName,lastName,email,phoneNumber,password,active,createdOn,lastLogin,locale," +
                "idEnterprise,birthday) VALUES (?,?,?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user.getFirstName());
            preparedStatement.setString(2,user.getLastName());
            preparedStatement.setString(3,user.getEmail());
            preparedStatement.setString(4,user.getPhoneNumber());
            preparedStatement.setString(5,user.getPassword());
            preparedStatement.setBoolean(6,user.getActive());
            preparedStatement.setDate(7,Date.valueOf(user.getCreatedOn().toLocalDate()));
            preparedStatement.setDate(8,Date.valueOf(user.getLastLogin().toLocalDate()));
            preparedStatement.setString(9,user.getLocale());
            preparedStatement.setInt(10,user.getIdEnterprise());
            preparedStatement.setDate(11,Date.valueOf(user.getBirthday()));
            preparedStatement.executeUpdate();

            return true;


        }catch (SQLException e){
            e.printStackTrace();
        }

        return false;

    }

    private boolean update(User user){

        Connection connection = this.mySQLConnection.getConnection();
        String sql = "UPDATE USER SET (firstName,lastName,email,phoneNumber,password,active,salt,createdOn,lastLogin,locale,idEnterprise,birthday)=(?,?,?,?,?,?,?,?,?,?,?,?)";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,user.getFirstName());
            preparedStatement.setString(2,user.getLastName());
            preparedStatement.setString(3,user.getEmail());
            preparedStatement.setString(4,user.getPhoneNumber());
            preparedStatement.setString(5,user.getPassword());
            preparedStatement.setBoolean(6,user.getActive());
            preparedStatement.setString(7,user.getSalt());
            preparedStatement.setDate(8,Date.valueOf(user.getCreatedOn().toLocalDate()));
            preparedStatement.setDate(9,Date.valueOf(user.getLastLogin().toLocalDate()));
            preparedStatement.setString(10,user.getLocale());
            preparedStatement.setInt(11,user.getIdEnterprise());
            preparedStatement.setDate(12,Date.valueOf(user.getBirthday()));
            preparedStatement.executeUpdate();

            return true;


        }catch (SQLException e){
            e.printStackTrace();
        }

        return false;

    }



}
