
import com.daniel.prog.connection.MySQLConnection;
import com.daniel.prog.model.DbUserRepository;
import com.daniel.prog.view.UserListControllerView;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {

    private Stage primaryStage;
    private DbUserRepository dbUserRepository;
    public Main() {
        MySQLConnection mySqlConnection = new MySQLConnection("crm_db", "root","micasabonita");
        this.dbUserRepository = new DbUserRepository(mySqlConnection);
    }
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("UserList");
        this.primaryStage.setResizable(false);
        showUserListScene();
    }
    /**
     * Shows the userList
     */
    public void showUserListScene() {
        try {
            //Instanciamos la clase que nos permitira cargar el fxml
            FXMLLoader loader = new FXMLLoader();
            //Cargamos el xml que hemos creado en el paso anterior
            loader.setLocation(Main.class.getResource("UserForm.fxml"));
            //Instanciamos El controlador de la vista y le inyectamos el repositorio de acceso a bd
            UserListControllerView userFormControllerView = new UserListControllerView(this.dbUserRepository);
            loader.setController(userFormControllerView);
            //cargamos la vista
            AnchorPane personOverview = loader.load();
            Scene scene = new Scene(personOverview);
            //la asociamos a la scena y la mostramos
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
